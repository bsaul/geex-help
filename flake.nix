{
  description = "geex office hours";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShells.default =  pkgs.mkShell {
        buildInputs = [
          # Documentation/writing tools
          pkgs.pandoc

          pkgs.R
          
          pkgs.rPackages.dplyr
          pkgs.rPackages.geex
          pkgs.rPackages.languageserver
          pkgs.rPackages.jsonlite
        ];
      }; 
    });
}
