# Weighted GLM

In `original.R` the `geex` user wanted to automate
weighted score functions for a generalized linear model
(see the `ScoreWTD` function).
The `geex` package contains the `grab_psiFUN` function
which will extract a score function from a `glm` model object;
however, this function does not support extracting the weights
and weighting the score function appropriately.
Luckily, this is easy to fix.
The trick is to extract the `glm` score function and compute the weights
in the estimating function and then do scalar/vector multiplication
of the score function by the weights.

The key lines in `updated.R` are in the `estfun_WTD` function:

```r
estfun_WTD <- \(data, models) {
  ...
  m_scores <- grab_psiFUN(models$m, data)

  \(theta){
    ...
    W  <- 1/dbinom(X, size = 1, prob = e)

    c(...
      W * m_scores(theta[m_pos]),
      ...
      )
  }
}
```
