library(geex)
library(boot)
library(stats)

#bring in example dataset:
### outcome=Y, exposure=X, covariates=(Z1, Z2, Z3)

### Estimate the effect of X on Y, assuming covariates Z1, Z2, Z3 provide conditional exchangeability.

dat<-read.csv('help/bonnie/exampledata.csv')


##########################

#weighted regression AIPW#

##########################


#fit propensity model

prop.model <- glm(X ~ Z1 + Z2 + Z1:Z2 + Z3 + Z1:Z3, family = binomial(), data = dat)
dat$PS<-predict(prop.model,dat, type="response")
dat$IPTW<-ifelse(dat$X==1,dat$PS^(-1),(1-dat$PS)^(-1))

#create copies of datasets for estimated potential outcomes

alltrt<-dat
alluntrt<-dat
alltrt$X<-1
alluntrt$X<-0


#fit outcome model (weighted)

out.model.wt <- glm(Y ~ Z1*Z2*X, weights=IPTW, data = dat)
dat$Y0.wt<-(predict(out.model.wt, alluntrt, type = "response"))
dat$Y1.wt<-(predict(out.model.wt, alltrt, type = "response"))

CM0.WTD<-mean(dat$Y0.wt)
CM1.WTD<-mean(dat$Y1.wt)
ATE.WTD<-CM1.WTD - CM0.WTD

#########################################################

##### geex functions

#########################################################

### this is the part I don’t want to have to specify

ScoreWTD<- function(X,Z1,Z2,Y,mu,W){
  c(W*(Y-mu),
    W*Z1*(Y-mu),
    W*Z2*(Y-mu),
    W*X*(Y-mu),
    W*Z1*Z2*(Y-mu),
    W*Z1*X*(Y-mu),
    W*Z2*X*(Y-mu),
    W*Z1*Z2*X*(Y-mu))}

estfun_WTD <- function(data,models){
  X<-data$X
  Z1<-data$Z1
  Z2<-data$Z2
  Y<-data$Y
  Xe <- grab_design_matrix(data = data, rhs_formula = grab_fixed_formula(models$e))
  Xm <- grab_design_matrix(data = data, rhs_formula = grab_fixed_formula(models$m))
  data0 <- data
  data0$X <- rep(0,nrow(data))
  Xm0 <- grab_design_matrix(data = data0,rhs_formula = grab_fixed_formula(models$m))
  data1 <- data
  data1$X <- rep(1,nrow(data))
  Xm1 <- grab_design_matrix(data = data1,rhs_formula = grab_fixed_formula(models$m))
  e_pos <- 1:ncol(Xe)
  m_pos <- (max(e_pos) + 1):(max(e_pos) + ncol(Xm))
  e_scores <- grab_psiFUN(models$e, data)

  function(theta){

    p <- length(theta)
    e <- plogis(Xe %*% theta[e_pos])
    mu <- Xm %*% theta[m_pos]
    m0 <- Xm0 %*% theta[m_pos]
    m1 <- Xm1 %*% theta[m_pos]
    W  < -X*e^(-1)+(1-X)*(1-e)^(-1)
    c(e_scores(theta[e_pos]),
      ScoreWTD(X,Z1,Z2,Y,mu,W),
      m1-theta[p-2],
      m0-theta[p-1],
      theta[p-2]- theta[p-1]-theta[p])
  }
}


geex_WTD <- function(data, propensity_formula, outcome_formula,coef.WTD, CM0.IV, CM1.IV, ATE.IV){

  e_model  <- glm(propensity_formula, data = data, family =binomial)
  m_model  <- glm(outcome_formula, data = data)
  models <- list(e = e_model, m=m_model)
  geex_resultsdrWTD <-m_estimate(
    estFUN = estfun_WTD,
    data   = data, 
    #root_control = setup_root_control(start = c(coef(e_model),coef.WTD, CM1.IV, CM0.IV, ATE.IV)),
    roots = c(coef(e_model),coef.WTD, CM1.IV, CM0.IV, ATE.IV),
    compute_roots = FALSE,
    outer_args = list(models = models))
  
  DR.WTD<-geex_resultsdrWTD@estimates[length(geex_resultsdrWTD@estimates)]
  seDR.WTD <- sqrt(geex_resultsdrWTD@vcov[length(geex_resultsdrWTD@estimates),length(geex_resultsdrWTD@estimates)])
  DR.WTD.all<-cbind(DR.WTD,seDR.WTD)
  return(DR.WTD.all)
}


#run geex to estimate SEs

DR.WTD <- geex_WTD(dat, X ~ Z1 + Z2 + Z1:Z2 + Z3 + Z1:Z3, Y ~ Z1*Z2*X, coef(out.model.wt), CM0.WTD, CM1.WTD, ATE.WTD)
DR.WTD

    # DR.WTD seDR.WTD
#  -16.09691  57.3878
