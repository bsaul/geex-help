# Reduce memory footprint

The [`geex`](https://github.com/bsaul/geex/blob/master/R/classes.R#L862) object
keeps *alot* of information,
most of which will be unncessary for a particular analysis.
The [`example.R`](example.R) script demonstrates
pulling out particular information to save space.
