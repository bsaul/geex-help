# G-computation

A user wanted to use G-computation for estimating causal effects
but struggled to write the estimating function
(see `orginal.R`) in a way
that predicted the potential outcomes
using a model fit on the full dataset.
The `updated.R` file contains a solution.
Note that this works because the user used a linear glm
(the default if you don't specify a distribution/link),
and thus the identity "link" function.

So this:

```r
 m0 <- Xm0 %*% theta[m_pos]
```

is essentially `predict` for the identity link.
But if you use a glm with a different link function,
you'd want to grab the link function from the model
in order to do something like:

```r
 m0 <- invLink(Xm0 %*% theta[m_pos])
```
